var expect = require('chai').expect;
var rewire = require('rewire');
var userRegistration = rewire('../lib/UserRegistration');
var sinon = require('sinon');                                   // require sinon

describe('method mock with sinon', function () {
    
    describe('testing user registerUser(user)', function() {
        
        var user = { name: 'Jean' };

        beforeEach(function(){
			userRegistration.__set__('UserRegistration.prototype.generateUserPassword', sinon.spy());	// use require to mock a method with sinon.spy function
			userRegistration.__set__('UserRegistration.prototype.register', sinon.stub().yields(333));	// com o stub você pode mockar a chamada de callbacks com respostas especificas
        });

        it('should call generateUserPassword() once', function () {
            userRegistration.registerUser(user);
            expect(userRegistration.generateUserPassword.callCount).to.equal(1);        // check for method calls, using spy properties
        });

        it('should call register(user) once with valid arguments', function() {
        	userRegistration.registerUser(user);

        	expect(userRegistration.register.calledOnce).to.equal(true);
        	expect(userRegistration.register.calledWith(user)).to.equal(true);			// check if method was called with valid arguments
        });

        it('should call callback with userId', function () {
        	var callback = sinon.spy();
        	userRegistration.registerUser(user, callback);
            expect(callback.calledWith(333)).to.equal(true);
        });
    });
});