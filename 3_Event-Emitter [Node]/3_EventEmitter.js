//ENTENDENDO O EVENT EMITTER

var eventos = require('events');
var emissor = new eventos.EventEmitter();           // EventEmitter é um objeto que lida com eventos

emissor.on('EventoCustom', function(message) {      // Para definir um novo evento, escolha um nome, e crie uma função de callback que será chamada
    console.log(`Evento Custom: ${message}`);
})

emissor.emit('EventoCustom', 'mensagem custom');    // O emit é usado para lançar um evento especifico

// CRIANDO UM OBJETO QUE HERDA DE EVENT EMITTER

var Pessoa = function() {
  this.nome = 'Jean';
  this.sobrenome = 'Toledo';
};

require('util').inherits(Pessoa, require('events').EventEmitter);       // mesma coisa que fizer o Pessoa.prototype = EventEmitter.prototype

var pessoa = new Pessoa();
pessoa.on('NomeCompleto', function() {                                  // definindo um custom event
    console.log(`Nome Completo: ${this.nome} ${this.sobrenome}`);
})

pessoa.emit('NomeCompleto');