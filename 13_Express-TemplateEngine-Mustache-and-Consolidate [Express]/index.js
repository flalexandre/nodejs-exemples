var cons = require('consolidate');                              // Template engine consolidation library for node.js
var express = require('express');
var app = express();

app.engine('html', cons.mustache);                              // Set html engine to match mustache
app.set('view engine', 'html');                                 // Set view engine as html
app.set('views', __dirname + '/public/views');                  // Set views directory

app.get('/', function (req, res) {
    res.render('index', { name: 'Jean Toledo' });               // send data as second argument to page
});

app.listen(5000);
console.log('server listenning at http://localhost:5000');