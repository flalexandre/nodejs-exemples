var cors = require('cors');
var bodyParser = require('body-parser');
var express = require('express');
var app = express();

var users = [
    { id : 1, name: 'Jean' },
    { id : 2, name: 'Douglas' }
]

app.use(cors());                                    // enable cross origin request

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));

app.get('/api/users', function (req, res) {
    res.json(users);                                // return json data
});

app.post('/api/users', function (req, res) {
    users.push(req.body);
    res.json(users);
});

app.listen(5000);
console.log('server listenning at http://localhost:5000/api');

module.exports = app;