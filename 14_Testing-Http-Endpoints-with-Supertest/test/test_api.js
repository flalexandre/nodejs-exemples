var request = require('supertest');             // supertest can be used to test http requests
var rewire = require('rewire');                 // rewire can be used to mock modules
var app = rewire('../server.js');               // load module with rewire

describe('testing users api', function () {
    
    beforeEach(function () {
        app.__set__('users', [
            { id: 10, name : 'teste' }
        ])
    });
    
    it('get request should work', function (done) {
        request(app).get('/api/users').expect(200).end(done);                                               // testing http get
    });
    
    it('get request should work', function (done) {
        request(app).post('/api/users').expect(200).send({ id: 11, name: 'teste2' }).end(done);             // testing http get
    });
});