var express = require('express');
var app = express();                                        // chame a fun��o express para criar uma nova app

app.use(function (req, res, next) {                         // � possivel criar um midleware customizado
    console.log(`${req.method} request for ${req.url}`);
    next();                                                 // sempre que um midleware terminar sua fun��o deve chamar o next
});                                                         // para chamar o pr�ximo midleware

app.use(express.static('./public'));                        // a fun��o use � utilizada para adicionar peda�os de
                                                            // midleware na app, o express j� vem com um midleware
                                                            // para servir arquivos est�ticos

app.listen(3000);                                           // use o m�todo listen para escolher a porta

module.exports = app;

console.log('server running at http://localhost:3000');

