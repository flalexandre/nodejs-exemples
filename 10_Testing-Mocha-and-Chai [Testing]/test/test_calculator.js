var expect = require('chai').expect;    // Assert framework for use with mocha http://chaijs.com/

describe("testing with mocha and chai", function() {
    
    describe("sum()", function () {                                     // simples expected test with mocha and chai
        it("should sum two numbers", function () {
            var soma = sum(1,2);
            expect(soma).to.equal(3);
        })
    });
    
    describe("sumAfterFourSeconds", function() {                        // testing async methods
        
        this.timeout(4500);                                             // por default o mocha aguarda 2 segundos antes de considerar o teste falho, esse método altera esse valor
        
        it("should sum two numbers after 4 seconds", function (done) {  // você pode receber um parametro done no "it" para dizer ao mocha que deve esperar a execução dos métodos
            sum4seconds(1, 2, function (result) {
                expect(result).to.equal(3);
                done();                                                 // chamar a função quando a framework deve realmente finalizar aquele teste
            });
        });
    })
})

function sum(a, b) { return a + b }; 
function sum4seconds(a, b, callback) {
    setTimeout(function() {
        callback(a + b);    
    }, 4000);
}; 