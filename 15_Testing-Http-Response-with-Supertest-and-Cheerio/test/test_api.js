var expect = require('chai').expect;
var request = require('supertest');             // supertest can be used to test http requests
var rewire = require('rewire');                 // rewire can be used to mock modules
var app = rewire('../server.js');               // load module with rewire
var cheerio = require('cheerio');

describe('testing users api', function () {
    
    var users = [{ id: 10, name : 'teste' }];
    
    beforeEach(function () {
        app.__set__('users', users)
    });
    
    it('get request should work', function (done) {
        request(app).get('/api/users').expect(200).end(done);                                               // testing http get
    });
    
    it('get request should work and return valid data', function (done) {
        request(app).post('/api/users').expect(200).send({ id: 11, name: 'teste2' })
            .end(function(err, res) {                                                                       // supertest will call an callback function with the response
                var data = JSON.parse(res.text);                                                            // you can parse response text to check json results
                expect(data).to.deep.equal(users);                                                          // chai to.deep.equal function compares if two object are exacle the same 
                done();                                                                                     // call done function for async tests
            });            
    });
    
    it('api page should have an h1 tag with "teste 123" text', function (done) {
        request(app).get('/api').expect(200).end(function (req, res) {
            var $ = cheerio.load(res.text);                                                                 // load response html with cheerio
            var h1Value = $('h1').text();                                                                   // search the DOM like jquery sintaxe
            
            expect(h1Value).to.equal('teste 123');                                                          // make expect using this values
            done();
        })
    })
});