var nome = "jean";

console.log(''); console.log('---------------- MEU HELLO WORLD--------------------'); console.log('');

console.log(`NOME PROJETO: Hello World ${nome}`); 			//só é possível fazer o escape de uma váriavel em uma string usando o ``
console.log("DIRETORIO: " + __dirname);
console.log("CAMINHO ARQUIVO: " + __filename);

var path = require('path'); //o modulo path vem com o node, pode ser usado para lidar com o sistema de arquivos

console.log("ARQUIVO: " + path.basename(__filename))

console.log(''); console.log('---------- TRABALHANDO COM VALORES ENVIADOS AO PROCESSO DO NODE -----------'); console.log('');

function obterParametroRecebido(param){
  var indexParam = process.argv.indexOf(param);				//todo parametro enviado na execução do modulo cai na variavel argv em forma de array
  return (indexParam === -1) ? null : process.argv[indexParam + 1];
}

console.log(`VALOR PARAMETRO NOME (Ex: node example.js --nome ValorQualquer): ${obterParametroRecebido('--nome')}`);

process.stdout.write(`\n---------- PROCESS stdin e stdout E EVENTOS ------------------`);

process.stdout.write(`\n\nVou te fazer algumas perguntas, ok?\n\n`);

var perguntas = [
    "\nQual seu nome ? > ",
    "\nQual seu sobrenome ? > "
];

var respostas = [];

function perguntar(index){
    process.stdout.write(perguntas[index]);
}

process.stdin.on('data', function(data){
  respostas.push(data.toString().trim());
  
  if(perguntas.length === respostas.length){
        process.exit()
    }else{
        perguntar(respostas.length);
    }
})

process.on('exit', function(){
   perguntas.length === respostas.length ? process.stdout.write(`\nSeu nome completo é: ${respostas[0]} ${respostas[1]}`) : '';
});

perguntar(0);