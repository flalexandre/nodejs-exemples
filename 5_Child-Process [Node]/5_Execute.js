var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

exec("git --version", function(err, stdout) {       // exec pode ser usado para executar comandos do windows ou linux, que seriam executados em console
    if(err) {
        throw err;
    }
    
    console.log(stdout);
})

// var executando = spawn('node', ['modulo_node'])                   o spawn pode ser usado assim como exec, porem ele funciona com processos longos e que lidam com mais dados

// executando.stdout.on(data, function(data){})                      você pode conversar com esse processo usando o stdout
// executando.stdin.write('teste')                                   e stdin

// executando.on('close', function(){})                              também pode executar algo quando o processo terminar