var ChatArea = {
    chatSymbol: '> ',
    setConectado: function (value) {
        var chatStatus = document.getElementById('chatStatus');
        if (value === true) {
            chatStatus.innerText = '(connected)';
            chatStatus.style.color = 'green';
        } else {
            chatStatus.innerText = '(disconected)';
            chatStatus.style.color = 'red';
        }
    },
    printMessage: function (message) {
        var _chatArea = document.getElementById('messages');
        var temp = document.createElement('p');
        temp.innerHTML = message;
        _chatArea.appendChild(temp);
    },
    choseUsernameCallback: function () { },
    sendMessageCallback: function () { }
}

window.onload = function () {
    configureUsername();
}

function configureUsername() {
    var choseUsername = document.getElementById('choseUsername');
    var username = document.getElementById('username');

    choseUsername.onclick = function () {
        if (username.value) {
            ChatArea.choseUsernameCallback(username.value);

            username.disabled = true;
            choseUsername.style.display = 'none';

            configureChatArea();
        }
    }
}

function configureChatArea() {
    document.getElementsByClassName('chatArea')[0].style.display = 'block';
    var myMessage = document.getElementById('myMessage');

    myMessage.value = ChatArea.chatSymbol;
    myMessage.focus();

    myMessage.onblur = function () {
        myMessage.focus();
    }

    myMessage.onkeydown = function (e) {
        if (e.keyCode === 8 && myMessage.value.length < 3) { //backspace
            myMessage.value = ChatArea.chatSymbol + ' ';
        }

        if (e.keyCode === 13 && myMessage.value !== ChatArea.chatSymbol) { // enter
            var message = myMessage.value;
            myMessage.value = ChatArea.chatSymbol;
            ChatArea.sendMessageCallback(message)
        }
    }
}