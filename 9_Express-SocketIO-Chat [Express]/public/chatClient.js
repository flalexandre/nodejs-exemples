var socket;

ChatArea.choseUsernameCallback = function (username) {
    socket = io('http://localhost:3000');
    socket.emit('setUsername', username);

    socket.on('connect', function () {
        ChatArea.setConectado(true);
    });

    socket.on('disconnect', function () {
        ChatArea.setConectado(false);
        ChatArea.printMessage('<strong>' + username + '</strong>, saiu do chat.');
    });

    socket.on('message', function (mensagem) {
        ChatArea.printMessage(mensagem);
    });

    ChatArea.sendMessageCallback = function (mensagem) {
        var msg = '<strong>' + username + ': </strong>' + mensagem.substring(2);
        socket.emit('chat', msg);
    }
};