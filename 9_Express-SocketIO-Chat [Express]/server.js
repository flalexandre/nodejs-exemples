var express = require('express');
var app = express();
var server = require('http').Server(app);           // um server http � criado a partir da aplica��o express, 
                                                    // os sockets s� conversam com server http
server.listen(3000);
console.log('server listening at http://localhost:3000');

app.use(express.static('./public'));                // usa a app para servir o site do chat
app.use(express.static('chatClient.js'));

// configura��o do server socket

var io = require('socket.io')(server);              // quando voc� chama a function do socket.io precisa enviar o server que esperar conex�es

io.on('connection', function (novoSocket) {         // � disparado sempre que um novo socket � conectado ao server
    novoSocket.on('chat', function (message) {
        io.sockets.emit('message', message);
    });

    novoSocket.on('disconnect', function () {
        var msg = '<strong>' + novoSocket.userName + '</strong>, saiu do chat.';
        io.sockets.emit('message', msg);
    })

    novoSocket.on('setUsername', function (value) {
        novoSocket.userName = value;
        io.sockets.emit('message', '<strong>' + novoSocket.userName + '</strong>, entrou no chat.');
    });
});
