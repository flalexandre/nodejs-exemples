var http = require('http');
var fs = require('fs');
var path = require('path');

var PORT = 3000;

http.createServer(function(req, res) {
    console.log(`${req.method} request for ${req.url}`);

    if(req.url === '/') {
        fs.readFile(path.join(__dirname, 'public/index.html'), 'UTF-8', function(err, html) {
            if(err) { 
                erro500(err, res); 
            } else {
                res.writeHead(200, { "Content-Type" : "text/html" });
                res.end(html);
            }
        });
    }
    else if(req.url.match(/.css$/)) {
        provideStaticFile(res, req.url, "text/css", true)
    } 
    else if(req.url.match(/.jpg$/)) {
        provideStaticFile(res, req.url, "image/jpeg", false)
    } 
    else {
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.end("file not found");  
    }
}).listen(PORT);

function provideStaticFile(res, reqUrl, mimeType, isTextFile) {
    var url = path.join(__dirname, 'public', reqUrl);
    var fileStream;
    
    if(isTextFile) {
        fileStream = fs.createReadStream(url, 'UTF-8');
    }
    else {
        fileStream = fs.createReadStream(url);
    }
    
    res.writeHead(200, { "Content-Type" : mimeType})
    fileStream.pipe(res);
}

function erro500(err, res) {
    res.writeHead(500, { "Content-Type" : "text/plain" });
    res.end('500 - Erro de Servidor : ' + err.message);
    console.log('500 - Erro de Servidor' + err.message);
}

console.log('Server Listenning on http://localhost:%d', PORT)