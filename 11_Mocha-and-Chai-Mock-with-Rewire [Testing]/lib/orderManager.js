var OrderManager = function() {};

var products = [];

OrderManager.prototype.orderIfStockEmpty = function (productId, qty) {
    for(var i=0; i<products.length; i++) {
        if(products[i].id === productId && products[i].qty <= 0) {
            products[i].qty += qty;
            break;
        }
    }
}

OrderManager.prototype.getProduct = function(productId) {
    var product;
    
     for(var i=0; i<products.length; i++) {
        if(products[i].id === productId) {
            product = products[i];
            break;
        }
    }
    
    return product;
}

module.exports = new OrderManager();