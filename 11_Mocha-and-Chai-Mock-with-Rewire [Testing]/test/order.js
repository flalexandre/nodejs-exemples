var expect = require('chai').expect;
var rewire = require('rewire');                                             // rewire is our mock test framework

var orderManager = rewire('../lib/orderManager');

var mockProducts = [                                                        // this is our fake data
    { id : 1, qty : 0 },
    { id : 2, qty : 3 }
]

describe("testing with mocha and chai", function() {
     
    describe("if stock is empty order more products", function () {
        
        beforeEach(function() {                                             // executes before each test
            orderManager.__set__("products", mockProducts);                 // set products array in module with a fake data
        });
        
        it("should order more products", function () {
            var product = orderManager.getProduct(1);
            
            orderManager.orderIfStockEmpty(1, 10);                          // the method that should order new products only if stock is empty
            
            expect(product.qty).to.equal(10);
        });
        
        it("should not order more products", function () {
            var product = orderManager.getProduct(2);
            
            orderManager.orderIfStockEmpty(1, 10);
            
            expect(product.qty).to.equal(3);
        });
        
    });
    
});