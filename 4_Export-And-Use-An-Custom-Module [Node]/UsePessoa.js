var Pessoa = require('./Lib/Pessoa');

var jean = new Pessoa('jean', 'toledo');

jean.on('nomeCompleto', function () {
    console.log(`${this.nome} ${this.sobrenome}`);
});

jean.emit('nomeCompleto');