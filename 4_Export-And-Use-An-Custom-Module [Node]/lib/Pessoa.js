var Pessoa = function(nome, sobrenome) {
    this.nome = nome;
    this.sobrenome = sobrenome;
}

Pessoa.prototype = require('events').EventEmitter.prototype;

module.exports = Pessoa;