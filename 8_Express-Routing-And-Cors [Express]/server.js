var express = require('express');
var cors = require('cors');                                 // cors é utilizado para permitir que ips remotos acessem nossa aplicação
var app = express();                                        // chame a função express para criar uma nova app

var nomes = [
    { nome: 'Jean', sobrenome: 'Toledo' },
    { nome: 'Sandra', sobrenome: 'Rossi' }
];

app.use(cors());                                            // cors retorna um midleware, então basta coloca-lo antes das rotas a serem configuradas

app.get('/api', function (req, res) {                       // O método get pode ser usado para configurar uma rota get
    res.json(nomes);
});

app.get('/api/:nome', function(req, res) {                  // route variavel pode ser criada com o :   
    res.json(nomes.filter(function(item){
        return item.nome.toLowerCase() ===          
            req.params.nome.toLowerCase();                  //o valor passado na url para variavel de rota pode ser acessado pelo req.params
    }));
});

// app.post pode ser usado para criar rotas de post
// app.delete pode ser usado para criar rotas de delete
// app.put pode ser usado para criar rotas de put

app.listen(3000);                                           // use o método listen para escolher a porta

module.exports = app;

console.log('server running at http://localhost:3000/api');

